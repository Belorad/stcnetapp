﻿namespace STCclient.Calculations
{
	internal class AmountOfDataCalc
	{
		internal void AddAmountOfData(int bytes, int portNumber, Dictionary<int, int> amountOfData)
		{
			try
			{
				amountOfData[portNumber] += bytes;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}
	}
}
