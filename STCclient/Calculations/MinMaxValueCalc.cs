﻿using STCclient.Models;

namespace STCclient.Calculations
{
	internal class MinMaxValueCalc
	{
		internal void GetMinValue(int resultInteger, int portNumber, Dictionary<int, MinValue> minValue)
		{
			try
			{
				if (minValue[portNumber].Value > resultInteger)
				{
					minValue[portNumber].PortNumber = portNumber;
					minValue[portNumber].Value = resultInteger;
				}
			}
			catch (Exception e)
			{
				minValue[portNumber].Value = null;
				Console.WriteLine(e.Message);
			}
		}

		internal void GetMaxValue(int resultInteger, int portNumber, Dictionary<int, MaxValue> maxValue)
		{
			try
			{
				if (maxValue[portNumber].Value < resultInteger)
				{
					maxValue[portNumber].PortNumber = portNumber;
					maxValue[portNumber].Value = resultInteger;
				}
			}
			catch (Exception e)
			{
				maxValue[portNumber].Value = null;
				Console.WriteLine(e.Message);
			}
		}
	}
}
