﻿using STCclient.Models;

namespace STCclient.Calculations
{
	internal class AverageValueCalc
	{
		internal void GetAverageValue(int resultInteger, int portNumber, Dictionary<int, AverageValue> averageValue)
		{
			try
			{
				if (averageValue[portNumber].Counter == 0)
				{
					averageValue[portNumber].Value = resultInteger;
					averageValue[portNumber].Counter++;
				}
				else
				{
					long? result = ((long)averageValue[portNumber].Value * averageValue[portNumber].Counter + resultInteger) / ++averageValue[portNumber].Counter;
					averageValue[portNumber].Value = (int)result;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}
	}
}
