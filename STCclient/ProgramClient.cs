﻿using STCclient;
using STCclient.DataView;
using STCclient.Models;

List<int> portNumbers = PortNumberEntering.EnterPortNumber();

List<Task> tasks = new(portNumbers.Count);
Client client = new();

bool switcherAB = true;

AverageValue averageValue = new();
MinValue minValue = new();
MaxValue maxValue = new();

Dictionary<int, AverageValue> averageValueA = new(portNumbers.Count); //<portNumber, Data>
Dictionary<int, MinValue> minValueA = new(portNumbers.Count); //<portNumber, Data>
Dictionary<int, MaxValue> maxValueA = new(portNumbers.Count); //<portNumber, Data>

Dictionary<int, AverageValue> averageValueB = new(portNumbers.Count); //<portNumber, Data>
Dictionary<int, MinValue> minValueB = new(portNumbers.Count); //<portNumber, Data>
Dictionary<int, MaxValue> maxValueB = new(portNumbers.Count); //<portNumber, Data>

Dictionary<int, int> amountOfDataIn5secA = new(portNumbers.Count); // <portNumber, amountOfData>
Dictionary<int, int> amountOfDataIn5secB = new(portNumbers.Count); // <portNumber, amountOfData>

Dictionary<int, int> amountOfDataInAllTime = new(portNumbers.Count); // <portNumber, amountOfData>

foreach (var portNumber in portNumbers)
{
	tasks.Add(new Task(() => client.CreateClient(ref switcherAB, portNumber, ref averageValueA, ref minValueA, ref maxValueA, ref averageValueB, ref minValueB, ref maxValueB, ref amountOfDataIn5secA, ref amountOfDataIn5secB, ref amountOfDataInAllTime)));

	averageValueA.Add(portNumber, new AverageValue());
	minValueA.Add(portNumber, new MinValue());
	maxValueA.Add(portNumber, new MaxValue());

	averageValueB.Add(portNumber, new AverageValue());
	minValueB.Add(portNumber, new MinValue());
	maxValueB.Add(portNumber, new MaxValue());

	amountOfDataIn5secA.Add(portNumber, 0);
	amountOfDataIn5secB.Add(portNumber, 0);

	amountOfDataInAllTime.Add(portNumber, 0);
}

foreach (var task in tasks)
{
	task.Start();
}

TimerCallback tc = new(OnTimedEvent);
using Timer timer = new(tc, null, 5000, 5000);

void OnTimedEvent(object? obj)
{
	ChangeSwitcher(ref switcherAB);

	if (!switcherAB)
	{
		//среднее текущее значение
		AverageValueGetter.GetAverageValue(ref averageValue, ref averageValueA);

		//текущие минимальное и максимальное значения за всё время
		MinMaxValuesGetter.GetMinMaxValues(ref minValue, ref maxValue,ref minValueA , ref maxValueA);
		
		//данные за последние 5 секунд
		AmountOfDataIn5secGetter.GetAmountOfDataIn5sec(ref amountOfDataIn5secA);
	}
	else if (switcherAB)
	{
		//среднее текущее значение
		AverageValueGetter.GetAverageValue(ref averageValue, ref averageValueB);

		//текущие минимальное и максимальное значения за всё время
		MinMaxValuesGetter.GetMinMaxValues(ref minValue, ref maxValue, ref minValueB, ref maxValueB);
		
		//данные за последние 5 секунд
		AmountOfDataIn5secGetter.GetAmountOfDataIn5sec(ref amountOfDataIn5secB);
	}

	//всего получено данных от каждого источника
	AmountOfDataInAllTimeGetter.GetAmountOfDataInAllTime(amountOfDataInAllTime);

	//количество активных источников данных
	AmountOfActiveSourcesGetter.GetAmountOfActiveSources(tasks);
	Console.WriteLine();
}

Task.WaitAll(tasks.ToArray());

void ChangeSwitcher(ref bool switcherAB)
{
	if (switcherAB)
	{
		switcherAB = false;
	}
	else
	{
		switcherAB = true;
	}
}