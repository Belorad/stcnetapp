﻿namespace STCclient.Models
{
	internal class MaxValue
	{
		internal int PortNumber { get; set; }
		internal int? Value { get; set; } = int.MinValue;
		internal bool IsActive { get; set; } = true;
		internal bool IsCalculate { get; set; } = true;
	}
}
