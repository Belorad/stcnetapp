﻿namespace STCclient.Models
{
	internal class AverageValue
	{
		internal int? Counter { get; set; } = 0;
		internal int? Value { get; set; }
		internal bool IsActive { get; set; } = true;
		internal bool IsCalculate { get; set; } = true;
	}
}
