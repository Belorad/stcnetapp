﻿namespace STCclient.Models
{
	internal class MinValue
	{
		internal int PortNumber { get; set; }
		internal int? Value { get; set; } = int.MaxValue;
		internal bool IsActive { get; set; } = true;
		internal bool IsCalculate { get; set; } = true;
	}
}
