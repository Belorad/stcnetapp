﻿namespace STCclient
{
	internal class PortNumberEntering
	{
		internal static List<int> EnterPortNumber()
		{
			Console.WriteLine("Введите по отдельности номера портов для подключения к серверам, в завершении введите два слэша вправо //:");

			List<int> ports = new();

			while (true)
			{
				string? input = Console.ReadLine();

				if (input == "//") break;

				bool result = int.TryParse(input, out int portNumber);

				if (result && portNumber > 0 && portNumber < 65536)
				{
					ports.Add(portNumber);
				}
				else
				{
					Console.WriteLine("Некорректный ввод номера порта! Повторите ввод, либо введите // для выхода.");
				}
			}

			return ports;
		}
	}
}
