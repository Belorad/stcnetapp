﻿using STCclient.Calculations;
using STCclient.Models;
using System.Net;
using System.Net.Sockets;

namespace STCclient
{
	internal class Client
	{
		internal void CreateClient(ref bool switcherAB, int portNumber, ref Dictionary<int, AverageValue> averageValueA, ref Dictionary<int, MinValue> minValueA, ref Dictionary<int, MaxValue> maxValueA, ref Dictionary<int, AverageValue> averageValueB, ref Dictionary<int, MinValue> minValueB, ref Dictionary<int, MaxValue> maxValueB, ref Dictionary<int, int> amountOfDataIn5secA, ref Dictionary<int, int> amountOfDataIn5secB, ref Dictionary<int, int> amountOfDataInAllTime)
		{
			AverageValueCalc averageValueCalc = new();
			MinMaxValueCalc minMaxValueCalc = new();
			AmountOfDataCalc amountOfDataCalc = new();
			
			using var tcpClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			try
			{
				IPAddress ip = IPAddress.Parse("127.0.0.1");

				tcpClient.Connect(ip, portNumber);

				while (true)
				{
					byte[] data = new byte[4];
					int bytes = tcpClient.Receive(data);
					int resultInteger = BitConverter.ToInt32(data);
					
					if (switcherAB)
					{
						//расчет нового среднего значения
						averageValueCalc.GetAverageValue(resultInteger, portNumber, averageValueA);

						//определение минимального значения
						minMaxValueCalc.GetMinValue(resultInteger, portNumber, minValueA);

						//определение максимального значения
						minMaxValueCalc.GetMaxValue(resultInteger, portNumber, maxValueA);

						//набор данных за 5 секунд
						amountOfDataCalc.AddAmountOfData(bytes, portNumber, amountOfDataIn5secA);

						//набор данных за всё время
						amountOfDataCalc.AddAmountOfData(bytes, portNumber, amountOfDataInAllTime);
					}
					else if (!switcherAB)
					{
						//расчет нового среднего значения
						averageValueCalc.GetAverageValue(resultInteger,portNumber, averageValueB);

						//определение минимального значения
						minMaxValueCalc.GetMinValue(resultInteger, portNumber, minValueB);

						//определение максимального значения
						minMaxValueCalc.GetMaxValue(resultInteger, portNumber, maxValueB);

						//набор данных за 5 секунд
						amountOfDataCalc.AddAmountOfData(bytes, portNumber, amountOfDataIn5secB);

						//набор данных за всё время
						amountOfDataCalc.AddAmountOfData(bytes, portNumber, amountOfDataInAllTime);
					}
				}
			}
			catch (Exception ex)
			{
				if (switcherAB)
				{
					averageValueA[portNumber].IsActive = false;
					minValueA[portNumber].IsActive = false;
					maxValueA[portNumber].IsActive = false;

					averageValueB[portNumber].IsActive = false;
					averageValueB[portNumber].IsCalculate = false;

					minValueB[portNumber].IsActive = false;
					minValueB[portNumber].IsCalculate = false;

					maxValueB[portNumber].IsActive = false;
					maxValueB[portNumber].IsCalculate = false;
				}
				else if (!switcherAB)
				{
					averageValueA[portNumber].IsActive = false;
					averageValueA[portNumber].IsCalculate = false;

					minValueA[portNumber].IsActive = false;
					minValueA[portNumber].IsCalculate = false;

					maxValueA[portNumber].IsActive = false;
					maxValueA[portNumber].IsCalculate = false;


					averageValueB[portNumber].IsActive = false;
					minValueB[portNumber].IsActive = false;
					maxValueB[portNumber].IsActive = false;
				}

				Console.WriteLine(ex.Message);
			}
		}
	}
}
