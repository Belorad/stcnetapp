﻿using STCclient.Models;

namespace STCclient.DataView
{
	internal class MinMaxValuesGetter
	{
		internal static void GetMinMaxValues(ref MinValue minValue, ref MaxValue maxValue, ref Dictionary<int, MinValue> minValueDict, ref Dictionary<int, MaxValue> maxValueDict)
		{
			Console.ForegroundColor = ConsoleColor.Green;

			foreach (var item in minValueDict)
			{
				if (item.Value.IsCalculate)
				{
					if (item.Value.Value != null && item.Value.IsActive == true)
					{
						if (item.Value.Value < minValue.Value)
						{
							minValue.Value = item.Value.Value;
							minValue.PortNumber = item.Value.PortNumber;
						}
					}
					else if (item.Value.Value != null && item.Value.IsActive == false)
					{
						if (item.Value.Value < minValue.Value)
						{
							minValue.Value = item.Value.Value;
							minValue.PortNumber = item.Value.PortNumber;
						}

						minValueDict[item.Key].Value = null;
						minValueDict[item.Key].IsCalculate = false;
					}

				}
				else if (!item.Value.IsCalculate)
				{
					if (item.Value.Value != null && item.Value.IsActive == false)
					{
						if (item.Value.Value < minValue.Value)
						{
							minValue.Value = item.Value.Value;
							minValue.PortNumber = item.Value.PortNumber;
						}

						minValueDict[item.Key].Value = null;
					}
				}
			}

			foreach (var item in maxValueDict)
			{
				if (item.Value.IsCalculate)
				{
					if (item.Value.Value != null && item.Value.IsActive == true)
					{
						if (item.Value.Value > maxValue.Value)
						{
							maxValue.Value = item.Value.Value;
							maxValue.PortNumber = item.Value.PortNumber;
						}
					}
					else if (item.Value.Value != null && item.Value.IsActive == false)
					{
						if (item.Value.Value > maxValue.Value)
						{
							maxValue.Value = item.Value.Value;
							maxValue.PortNumber = item.Value.PortNumber;
						}

						maxValueDict[item.Key].Value = null;
						maxValueDict[item.Key].IsCalculate = false;
					}

				}
				else if (!item.Value.IsCalculate)
				{
					if (item.Value.Value != null && item.Value.IsActive == false)
					{
						if (item.Value.Value > maxValue.Value)
						{
							maxValue.Value = item.Value.Value;
							maxValue.PortNumber = item.Value.PortNumber;
						}

						maxValueDict[item.Key].Value = null;
					}
				}
			}

			Console.WriteLine($"Порт номер {minValue.PortNumber}. Минимальное текущее значение равно {minValue.Value}.\n" +
				$"Порт номер {maxValue.PortNumber}. Максимальное текущее значение равно {maxValue.Value}.");
			Console.ResetColor();
		}
	}
}
