﻿using STCclient.Models;

namespace STCclient.DataView
{
	internal class AverageValueGetter
	{
		internal static void GetAverageValue(ref AverageValue averageValue, ref Dictionary<int, AverageValue> averageValueDict)
		{
			Console.ForegroundColor = ConsoleColor.Green;

			long? sum = 0;
			int? activeSourceCounter = 0;
			long? subResult;

			if (averageValue.Counter == 0)
			{
				subResult = 0;
			}
			else
			{
				subResult = (long)averageValue.Value * averageValue.Counter;
			}

			foreach (var item in averageValueDict)
			{
				if (item.Value.IsCalculate)
				{
					if (item.Value.Value != null && item.Value.Counter != null && item.Value.IsActive == true)
					{
						sum += item.Value.Value;
						activeSourceCounter++;
						averageValue.Counter += item.Value.Counter;

						long? result = (subResult + sum * activeSourceCounter) / averageValue.Counter;
						averageValue.Value = (int)result;
					}
					else if (item.Value.Value != null && item.Value.Counter != null && item.Value.IsActive == false)
					{
						sum += item.Value.Value;
						activeSourceCounter++;
						averageValue.Counter += item.Value.Counter;
						long? result = (subResult + sum * activeSourceCounter) / averageValue.Counter;
						averageValue.Value = (int)result;

						averageValueDict[item.Key].Value = null;
						averageValueDict[item.Key].Counter = null;
						averageValueDict[item.Key].IsCalculate = false;
					}
				}
				else if (!item.Value.IsCalculate)
				{
					if (item.Value.Value != null && item.Value.Counter != null && item.Value.IsActive == false)
					{
						sum += item.Value.Value;
						activeSourceCounter++;
						averageValue.Counter += item.Value.Counter;
						long? result = (subResult + sum * activeSourceCounter) / averageValue.Counter;
						averageValue.Value = (int)result;

						averageValueDict[item.Key].Value = null;
						averageValueDict[item.Key].Counter = null;
					}
				}
			}

			Console.WriteLine($"Среднее текущее значение равно {averageValue.Value}.");
			Console.ResetColor();
		}
	}
}
