﻿namespace STCclient.DataView
{
	internal class AmountOfDataIn5secGetter
	{
		internal static void GetAmountOfDataIn5sec(ref Dictionary<int, int> amountOfDataIn5sec)
		{
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine("За последние 5 секунд получено данных от каждого источника:");

			foreach (var amount in amountOfDataIn5sec)
			{
				if (amount.Value < 1024)
				{
					Console.WriteLine($"От источника порт номер {amount.Key} получено {amount.Value} Б.");
				}
				else if (amount.Value >= 1024 && amount.Value < 1048576)
				{
					int amountOfData = amount.Value / 1024;
					Console.WriteLine($"От источника порт номер {amount.Key} получено {amountOfData} КБ ({amount.Value} Б).");
				}
				else
				{
					int amountOfData = amount.Value / 1048576;
					int amountOfDataKB = amount.Value / 1024;
					Console.WriteLine($"От источника порт номер {amount.Key} всего получено {amountOfData} МБ ({amountOfDataKB} КБ или {amount.Value} Б).");
				}

				amountOfDataIn5sec[amount.Key] = 0;
			}

			Console.ResetColor();
		}
	}
}
