﻿namespace STCclient.DataView
{
	internal class AmountOfActiveSourcesGetter
	{
		internal static void GetAmountOfActiveSources(List<Task> tasks)
		{
			int amount = 0;

			foreach (var task in tasks)
			{
				if (task.Status == TaskStatus.Running)
				{
					amount++;
				}
			}

			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine($"Количество активных источников на данный момент равно {amount}.");
			Console.ResetColor();
		}
	}
}
