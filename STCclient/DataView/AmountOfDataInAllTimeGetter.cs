﻿namespace STCclient.DataView
{
	internal class AmountOfDataInAllTimeGetter
	{
		internal static void GetAmountOfDataInAllTime(Dictionary<int, int> amountOfDataInAllTime)
		{
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine("Всего получено данных от каждого источника:");

			foreach (var amount in amountOfDataInAllTime)
			{
				if (amount.Value < 1024)
				{
					Console.WriteLine($"От источника порт номер {amount.Key} всего получено {amount.Value} Б.");
				}
				else if (amount.Value >= 1024 && amount.Value < 1048576)
				{
					int amountOfData = amount.Value / 1024;
					Console.WriteLine($"От источника порт номер {amount.Key} всего получено {amountOfData} КБ ({amount.Value} Б).");
				}
				else
				{
					int amountOfData = amount.Value / 1048576;
					int amountOfDataKB = amount.Value / 1024;
					Console.WriteLine($"От источника порт номер {amount.Key} всего получено {amountOfData} МБ ({amountOfDataKB} КБ или {amount.Value} Б).");
				}
			}

			Console.ResetColor();
		}
	}
}
