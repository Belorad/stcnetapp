﻿namespace STCserver
{
	internal class RandomInteger
	{
		internal static int GetRandomInteger()
		{
			var random = new Random();
			int randomInt = random.Next(int.MinValue, int.MaxValue);

			return randomInt;
		}
	}
}
