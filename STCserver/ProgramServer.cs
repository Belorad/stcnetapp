﻿using STCserver;
using System.Net;
using System.Net.Sockets;

IPEndPoint ipPoint = ServerPortNumberChecking.CheckServerPortNumber();

using Socket tcpListener = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

Server.CreateServer(ipPoint, tcpListener);