﻿using System.Net;
using System.Net.Sockets;

namespace STCserver
{
	internal class Server
	{
		internal static void CreateServer(IPEndPoint ipPoint, Socket tcpListener)
		{
			try
			{
				tcpListener.Bind(ipPoint);
				tcpListener.Listen();
				Console.WriteLine("Сервер запущен.");
				Console.WriteLine($"Адресс сервера: {tcpListener.LocalEndPoint}; сервер ожидает подключений...");

				int integer = 0;
				int interval = 0;

				while (true)
				{
					using var tcpClient = tcpListener.Accept();

					if (tcpClient.Connected)
					{
						Console.WriteLine($"Клиент {tcpClient.RemoteEndPoint} подключен.");
					}

					while (true)
					{
						integer = RandomInteger.GetRandomInteger();
						interval = RandomInterval.GetRandomInterval();

						byte[] data = BitConverter.GetBytes(integer);

						Thread.Sleep(interval);

						int bytes = tcpClient.Send(data);
						Console.WriteLine($"Данные отправлены клиенту {tcpClient.RemoteEndPoint}, число равно {integer}, количество данных {bytes} Б.");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
