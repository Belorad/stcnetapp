﻿using System.Net;
using System.Net.NetworkInformation;

namespace STCserver
{
	internal class ServerPortNumberChecking
	{
		internal static IPEndPoint CheckServerPortNumber()
		{
			Console.WriteLine("Активные TCP-соединения по адресу 127.0.0.1:");
			IPAddress ip = IPAddress.Parse("127.0.0.1");
			IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
			TcpConnectionInformation[] connections = properties.GetActiveTcpConnections();

			bool isActiveTCP = false;

			foreach (TcpConnectionInformation c in connections)
			{
				if (c.LocalEndPoint.Address.Equals(ip))
				{
					isActiveTCP = true;
					Console.WriteLine("{0} <==> {1}",
						c.LocalEndPoint.ToString(),
						c.RemoteEndPoint.ToString());
				}
			}

			if (!isActiveTCP)
			{
				Console.WriteLine("активных TCP-соединений по адресу 127.0.0.1 нет.");
			}

			IPEndPoint ipPoint;

			Console.WriteLine("Введите номер порта сервера:");

			while (true)
			{
				string? input = Console.ReadLine();

				bool result = int.TryParse(input, out int port);

				if (result && port > 0 && port < 65536)
				{
					ipPoint = new(ip, port);
					bool isActive = false;

					foreach (var c in connections)
					{
						if (c.LocalEndPoint.Equals(ipPoint) || c.RemoteEndPoint.Equals(ipPoint))
						{
							isActive = true;
						}
					}

					if (isActive)
					{
						Console.WriteLine("Данный порт по адресу 127.0.0.1 занят! Повторите ввод.");
					}
					else
					{
						break;
					}
				}
				else
				{
					Console.WriteLine("Некорректный ввод номера порта! Повторите ввод.");
				}
			}

			return ipPoint;
		}
	}
}
