﻿namespace STCserver
{
	internal class RandomInterval
	{
		internal static int GetRandomInterval()
		{
			var random = new Random();
			var randomInterval = random.Next(1, 1000);

			return randomInterval;
		}
	}
}
